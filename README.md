# Requirements

- `sic`: http://tools.suckless.org/sic/
- `xmlstarlet`: http://xmlstar.sourceforge.net/

# Configuration

```
cp config.defaults config
```

then modify to set the bot's channel, nick, etc.

Create a file called `feeds` and put the URLs of the feeds you want to
subscribe to in it, one per line.

# Running

`./umbra`
