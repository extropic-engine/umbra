
# doing

- [ ] use named pipes for i/o
- [ ] add gitlab CI runner to validate with shellcheck
- [ ] minecraft bridge, see https://github.com/minefold/hubot-minecraft as a starting point
- [ ] add flag to immortal to allow naming jobs

# done

- [X] include post title and author in message if available
- [X] look at xmlstarlet for advanced rss processing: http://xmlstar.sourceforge.net/docs.php
